const path = require(`path`)

const { browserify } = require(`@suzan_pevensive/browserify-js`)

module.exports = {

  promise: require(`./src/promise`),
  tryAwait: require(`./src/tryAwait`),
  wait: require(`./src/wait`),
  task: require(`./src/task`),

  browserify: (app) => browserify.publishModule(app, `SafeAsyncJs`, path.join(__dirname, `src`), `js`)

}
