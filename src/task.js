const promise = require('./promise')

/**

  @description
    safe invoke [function] as async and return thier result in promise

  @param {function}   task - function to safe async invoke
  @param {array}      args - array of arguments to invoke [function]
  @return {promise}

  @example

    function someFunction(options){

      ...
      return `done`

    }

    let result = await task(someFunction, [{ type: `...` }])
    result == `done`

*/
module.exports = function(task, args){
  return promise((resolve, reject)=>{
    resolve(task(...args))
  })
}
