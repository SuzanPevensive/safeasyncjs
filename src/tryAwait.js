/**

  @description
    invoke async [function] and return thier result or [null] if [function] thrown
      the [err] libary field contains last throwing message

  @param {function}   action - async function to safe invoke
  @param {array}      args - array of arguments to invoke [function]
  @return {any}

  @example

    async function login(username, password){

      if(!username) throw `username is empty`
      if(!password) throw `password is empty`

      ...

    }

    let result = tryAwait(login, [`Suzan`, ``])
    result == null

    tryAwait.err == `password is empty`

*/
let tryAwait = async function(func, args = []){
  tryAwait.err = null
  try{
    return await func(...args)
  }catch(e){
    tryAwait.err = e
    return null
  }
}
module.exports = tryAwait
