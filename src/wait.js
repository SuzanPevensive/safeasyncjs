const promise = require('./promise')

/**

  @description
    waits for the specified [milis] while executing an async function

  @param {number}   milis - milis to waiting
  @return {promise}

  @example "correct"

    let start = Date.now()
    await wait(2000)
    let result = Date.now() - start
    result ≈≈ 2000

*/
module.exports = function(milis){
  return promise((resolve, reject)=>{
    setTimeout(()=>{ resolve() }, milis)
  })
}
