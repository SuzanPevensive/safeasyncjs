const tryAwait = require('./tryAwait');

/**

  @description
    return result of [promise] whitch implementetd argument [function]
      if [promise] reject return null
      the [err] tryAwait libary field contains last throwing message

  @param {function}   action - function to implemented as promise body
  @return {promise}

  @example "correct"

    let resultPromise = promise((resolve, reject)=>{
      ...
      resolve(12)
    })
    let result = await resultPromise()
    result == 12

  @example "incorrect"

    let resultPromise = promise((resolve, reject)=>{
      ...
      reject(`error ...`)
    })
    let result = await resultPromise()
    result == null

    tryAwait.err == `error ...`

*/
module.exports = async function(action){
  return await tryAwait(()=>(new Promise(action)))
}
